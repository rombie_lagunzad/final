<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Random" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 12/5/2020
  Time: 7:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Subscribe Now</title>
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <img src="https://i.ibb.co/WWXR8m7/recipe.jpg">
    <div class="text-container">
        <div class="title-container">
            <h1 class="heading">Your Daily Recipe</h1>
        </div>
        <div class="p1-container">
            <h2 class="sub-heading">
                Subscribe to our newsletter to receive our flavorful recipes!
            </h2>
        </div>
        <div class="p2-container">
            <form id="contact" action="SubscribeNow" method="post">
                <input name="name" placeholder="Your First Name" type="text" tabindex="1" required autofocus>
                <input name="email" placeholder="Your Email Address" type="email" tabindex="2" required>
                <input name="country" placeholder="Country You Reside" type="text" tabindex="3" required>
                <span>
                </span>

                <input class="captcha-answer" name="captcha" placeholder="captcha" type="text" tabindex="4" required>
                <button name="submit" type="submit" class="contact-submit" >SUBSCRIBE NOW</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>
