import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;

import java.util.List;

public class TestNow {

    SessionFactory factory = null;
    Session session = null;

    private static TestNow single_instance = null;

    private TestNow() {
        factory = HibernateUtility.getSessionFactory();
    }

    public static TestNow getInstance() {
        if (single_instance == null) {
            single_instance = new TestNow();
        }
        return single_instance;
    }


    public List<Emails> getEmail() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Emails";
            List<Emails> cs = (List<Emails>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            System.out.println("Out of Range: Failed to open session!");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Emails getEmail(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Emails where id = " + (id);
            Emails c = (Emails) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            System.out.println("Out of Range: Failed to open session!");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}