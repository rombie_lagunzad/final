import javax.persistence.*;

@Entity
@Table(name="emaillist")

public class Emails {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "ID")
    private int id;

    @Column(name = "FirstName")
    private String name;

    @Column(name = "Email")
    private String email;

    @Column(name = "Country")
    private String country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return name;
    }

    public void setFirstName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String toString() {

        return "\nName: " + name + " |    Email Address: " + email + "   | Country: " +
                country;
    }
}
