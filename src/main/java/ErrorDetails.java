import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ErrorDetails", urlPatterns={"/ErrorDetails"})
public class ErrorDetails extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processError(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        processError(request, response);
    }

    private void processError(HttpServletRequest request,
                              HttpServletResponse response) throws IOException {

        // Check the servlet exception
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
        if (servletName == null) {
            servletName = "Unknown";
        }
        String requestUrl = (String) request.getAttribute("javax.servlet.error.request_uri");
        if (requestUrl == null) {
            requestUrl = "Unknown";
        }

        // Set response content per type of error
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.write("<html><head><title>Error Details</title><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
        if(statusCode != 500){
            out.write("<h1>Error Details</h1>");
            out.write("<p><strong>Status Code</strong>: "+statusCode+"</p>");
            out.write("<p><strong>Requested URL</strong>: "+requestUrl+"</p>");
            if (statusCode == 404) {
                out.write("<p>Page not found! Check the URL again.</p>");
            }
            else if (statusCode == 403) {
                out.write("<p>You are forbidden to view this page.</p:");
            }
            else if (statusCode == 401) {
                out.write("<p>You are don't have a valid authentication credentials to access the page.</p>");
            }
            else {
                out.write("<p>Please contact the administrator if you are seeing this error.</p>");
            }
        }

        else{
            out.write("<h1>Exception Details</h1>");
            out.write("<ul><li>Servlet Name:"+servletName+"</li>");
            out.write("<li>Exception Name:"+throwable.getClass().getName()+"</li>");
            out.write("<li>Requested URL:"+requestUrl+"</li>");
            out.write("<li>Exception error occurred. We apologize!</li>");
            out.write("</ul>");
        }

        out.write("<br>");
        out.write("<a class=\"click-here\" href=\"http://localhost:8080/final_war_exploded/index.html\">HOMEPAGE</a>");
        out.write("</body></html>");
    }
}