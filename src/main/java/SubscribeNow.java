import org.hibernate.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "SubscribeNow", urlPatterns={"/SubscribeNow"})
public class SubscribeNow extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Retrieve the input information and save it
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String country = request.getParameter("country");
        Integer sum = Integer.parseInt(request.getParameter("addition"));

        // Java collection using Map interface
        Map<Integer, String> recipe = new HashMap<>();
        recipe.put(1, "https://www.allrecipes.com/recipe/238691/simple-macaroni-and-cheese/");
        recipe.put(2, "https://www.delish.com/cooking/recipe-ideas/a19665622/easy-chicken-fajitas-recipe/");
        recipe.put(3, "https://www.thedailymeal.com/best-recipes/creamy-carrot-soup");
        recipe.put(4, "https://www.food.com/recipe/absolute-best-ever-lasagna-28768");
        recipe.put(5, "https://www.tasteofhome.com/recipes/caramel-pecan-rolls/");


        // Data validation with validator
        if (sum != 5) {
            PrintWriter errorCaptcha = response.getWriter();
            response.setContentType("text/html");
            errorCaptcha.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
            errorCaptcha.println("<h2>The sum you have entered is incorrect. Please try again");
            errorCaptcha.println("<br>");
            errorCaptcha.println("<br>");
            errorCaptcha.println("<a class=\"click-here\" href=\"http://localhost:8080/final_war_exploded/SubscribeNewsletter.html\">GO BACK</a>");
            errorCaptcha.println("</body></html>");
        }

        // Add the details to the database
        else {

            try {
                Session session = HibernateUtility.getSessionFactory().openSession();
                session.beginTransaction();

                Emails lead = new Emails();
                lead.setFirstName(name);
                lead.setEmail(email);
                lead.setCountry(country);

                // Save the items in the database
                session.save(lead);

                // Commit the transaction
                session.getTransaction().commit();

                // Make a SQL query to retrieve the last created ID
                Object result = session.createSQLQuery("SELECT id FROM emaillist ORDER BY id DESC LIMIT 1;")
                        .uniqueResult();
                int lastID = (Integer) result;

                // Use the latest ID to retrieve the information
                String sql = "from Emails where id = " + (lastID);
                Emails lastEmail = (Emails) session.createQuery(sql).getSingleResult();

                // Shutdown the session
                HibernateUtility.shutdown();

                // Display the new content of the page
                PrintWriter out = response.getWriter();
                response.setContentType("text/html");
                out.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");

                out.println("<h1>Thank you for subscribing, " + name + "!</h1>");
                out.println("<h2>We have added you to our mailing list. Here's the contact details we added:</h2>");
                out.println("<p>" + lastEmail + "</p><br/>");
                out.println("<h2>As a gift, check below the previous recipes we sent out!</p>");
                out.println("<p><a href=\"" + recipe.get(1) + "\">Simple Macaroni and Cheese</a></p>");
                out.println("<p><a href=\"" + recipe.get(2) + "\">Chicken Fajitas</a></p>");
                out.println("<p><a href=\"" + recipe.get(3) + "\">Creamy Carrot Soup</a></p>");
                out.println("<p><a href=\"" + recipe.get(4) + "\">Easy Lasagna</a></p>");
                out.println("<p><a href=\"" + recipe.get(5) + "\">Caramel Pecan Rolls</a></p>");
                out.println("</body></html>");

                // Notification of leads coming in
                System.out.println("\n***Lead Notification***");
                System.out.println(lastEmail);
            }

            catch(Exception e){
                    PrintWriter out = response.getWriter();
                    response.setContentType("text/html");
                    out.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
                    out.println("<h1>There's an error in adding the email</h1>");
                    out.println("</body></html>");
                }

            }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter errorPage = response.getWriter();
        errorPage.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
        errorPage.println("<h1>Page Not Found</h1>");
        errorPage.println("<a class=\"click-here\" href=\"http://localhost:8080/final_war_exploded/SubscribeNewsletter.html\">GO BACK</a>");
        errorPage.println("</body></html>");

    }
}
